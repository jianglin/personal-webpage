<?php 
$name = $email = $message = ''; 

if(isset($_POST["name"])) $name = $_POST["name"]; 
if(isset($_POST["email"])) $email = $_POST["email"]; 
if(isset($_POST["message"])) $message = $_POST["message"]; 

date_default_timezone_set('America/New_York');
$time = date("Y-m-d H:i:s");
$file_name = $email?: 'anonymous';
if ($message <> ''){ 
    $fp  = fopen('messages/' . $file_name . '.txt', 'a+'); 
    fwrite($fp, "==========\n" . ($name?:'anonymous') . ' (' . $email . ') @ ' . $time . "\n" . $message . "\n\n"); 
    fclose($fp); 
} 